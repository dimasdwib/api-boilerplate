<?php

Route::get('/', function() {
  echo 'web api';
});

/**
 * API Routing
 */
 RouteApi::version('v1',  function() {
   RouteApi::group(['prefix' => 'auth', 'namespace' => 'App\Http\Controllers\Auth'], function() {
    RouteApi::get('/me', 'LoginController@me');
    RouteApi::post('/login', 'LoginController@login');
    RouteApi::post('/logout', 'LoginController@logout');
  });
});
