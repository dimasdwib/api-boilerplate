<?php
namespace Modules\User\Entities;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class User extends Model {
  use HasRoles;

  protected $guard_name = 'web';

  const UPDATED_AT = 'last_update';
  protected $table = 'users';
}
