<?php

Route::group(['middleware' => 'web', 'prefix' => 'user', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::get('/', 'UserController@index');
});

RouteApi::version('v1',  function() {
  RouteApi::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'user',
    'namespace' => 'Modules\User\Http\Controllers']
  , function() {
   RouteApi::get('/', 'UserController@index');
   RouteApi::get('/me', 'UserController@me');
   RouteApi::get('/get/{id}', 'UserController@get_user');
   RouteApi::get('/all', 'UserController@get_all_user');
   RouteApi::post('/store', 'UserController@store');
   RouteApi::post('/update/{id}', 'UserController@update');
   RouteApi::post('/delete/{id}', 'UserController@delete');
 });
});
