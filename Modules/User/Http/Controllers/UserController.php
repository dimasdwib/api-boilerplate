<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Modules\User\Entities\User;
use DB;
use JWTAuth;

class UserController extends Controller
{

    public function index() {
      return DB::table('users')->paginate(10);
    }

    public function get_all_user() {
      return DB::table('users')->get();
    }

    public function me() {
      try {

    		if (! $user = JWTAuth::parseToken()->authenticate()) {
    			return response()->json(['user_not_found'], 404);
    		}

    	} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

    		return response()->json(['token_expired'], $e->getStatusCode());

    	} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

    		return response()->json(['token_invalid'], $e->getStatusCode());

    	} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

    		return response()->json(['token_absent'], $e->getStatusCode());

    	}

    	// the token is valid and we have found the user via the sub claim
    	return response()->json(['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
      $name = $request->name;
      $username = $request->username;
      $email = $request->email;
      $password = $request->password;
      $permission = explode(',', $request->permission );
      $role = explode(',', $request->role);

      $id_user = User::insertGetId([
          'name' => $name,
          'username' => $username,
          'email' => $email,
          'password' => bcrypt($password),
          'created_at' => date('Y-m-d H:i:s'),
        ]);

      $user = User::find($id_user);
      foreach ($role as $key => $r) {
        $user->assignRole($r);
      }
      foreach ($permission as $key => $p) {
        $user->givePermissionTo($p);
      }

      if ($id_user) {
        return response()->json([
          'message' => 'User has been created succesfully',
          'status' => 'success',
          'id_user' => $user,
        ]);
      }

      return response()->json([
        'message' => 'Failed create user',
        'status' => 'error',
      ], 500);

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function get_user(Request $request, $id) {
      $user = User::find($id);
      if ($user) {

        $permission = $user->getDirectPermissions();
        $roleNames = $user->getRoleNames();

        $role = DB::table('roles')
                  ->select('name as label', 'id as value')
                  ->whereIn('name', $roleNames)
                  ->get();

        return response()->json([
          'message' => 'success',
          'status' => 'success',
          'data' => $user,
          'permission' => $permission,
          'role' => $role,
        ]);
      }

      return response()->json([
        'message' => 'Failed get user',
        'status' => 'error',
      ], 500);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
      $name = $request->name;
      $username = $request->username;
      $email = $request->email;
      $permission = explode(',', $request->permission);
      $role = explode(',', $request->role);

      $user = User::find($id);
      if ($request->role != '') {
        $user->syncRoles($role);
      }

      if ($request->permission != '') {
        $user->syncPermissions($permission);
      }

      $data = [
        'name' => $name,
        'username' => $username,
        'email' => $email,
      ];

      /* if change password */
      if ($request->password != '') {
        $data['password'] = bcrypt($request->password);
      }

      $update = User::where('id', $id)
                ->update($data);

      if ($update > 0) {
        return response()->json([
          'message' => 'User has been updated succesfully',
          'status' => 'success',
          'id' => $id,
        ]);
      }

      return response()->json([
        'message' => 'Nothing updated',
        'status' => 'error',
        'id' => $id,
      ], 500);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function delete(Request $request, $id) {
      if ($id == 1) {
        return response()->json([
          'message' => 'Can\'t delete Administrator',
          'status' => 'error',
        ], 500);
      }
      $user = User::find($id);

      /* Remove all permission & role */
      $role = Role::all();
      $permission = Permission::all();
      foreach($role as $r) {
        $user->removeRole($r->name);
      }
      foreach($permission as $p) {
        $user->revokePermissionTo($p->name);
      }

      $delete = DB::table('users')
                ->where('id', $id)
                ->delete();
      if ($delete) {
        return response()->json([
          'message' => 'User has been deleted successfully',
          'status' => 'success',
        ]);
      }

      return response()->json([
        'message' => 'Failed delete user',
        'status' => 'error',
      ], 500);
    }
}
