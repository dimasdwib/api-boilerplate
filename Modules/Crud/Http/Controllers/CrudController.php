<?php

namespace Modules\Crud\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class CrudController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return DB::table('crud')->get();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function store(Request $request)
    {
      $name = $request->name;
      $email = $request->email;
      $address = $request->address;

      $id = DB::table('crud')
              ->insertGetId([
                'name' => $name,
                'email' => $email,
                'address' => $address,
              ]);

      if ($id) {
        return response()->json([
          'message' => 'Data has been created',
          'id' => $id,
          'status' => 'success',
        ]);
      }

      return response()->json([
        'message' => 'Error',
        'status' => 'error',
      ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
      $name = $request->name;
      $email = $request->email;
      $address = $request->address;

      $update = DB::table('crud')
              ->where('id', $id)
              ->update([
                'name' => $name,
                'email' => $email,
                'address' => $address,
              ]);

      if ($update > 0) {
        return response()->json([
          'message' => 'Data has been updated',
          'id' => $id,
          'status' => 'success',
        ]);
      }

      return response()->json([
        'message' => 'Error',
        'status' => 'error',
      ]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function delete(Request $request, $id) {
      $delete = DB::table('crud')
                  ->where('id', $id)
                  ->delete();

      if ($delete) {
        return response()->json([
          'message' => 'Data has been deleted',
          'status' => 'success',
        ]);
      }

      return response()->json([
        'message' => 'Error',
        'status' => 'error',
      ]);
    }


}
