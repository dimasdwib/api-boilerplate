<?php

Route::group(['middleware' => 'web', 'prefix' => 'crud', 'namespace' => 'Modules\Crud\Http\Controllers'], function()
{
    Route::get('/', 'CrudController@index');
});

RouteApi::version('v1',  function() {
  /* Permission */
  RouteApi::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'crud',
    'namespace' => 'Modules\Permission\Http\Controllers']
  , function() {
   RouteApi::get('/', 'CrudController@index');
   RouteApi::get('/get/{id}', 'CrudController@get_crud');
   RouteApi::post('/store', 'CrudController@store');
   RouteApi::post('/update/{id}', 'CrudController@update');
   RouteApi::post('/delete/{id}', 'CrudController@delete');

 });
});
