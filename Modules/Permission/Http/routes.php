<?php

Route::group(['middleware' => 'web', 'prefix' => 'permission', 'namespace' => 'Modules\Permission\Http\Controllers'], function()
{
    Route::get('/', 'PermissionController@index');
});

RouteApi::version('v1',  function() {
  /* Permission */
  RouteApi::group([
    'middleware' => 'jwt.auth',
    'prefix' => 'permission',
    'namespace' => 'Modules\Permission\Http\Controllers']
  , function() {
   RouteApi::get('/', 'PermissionController@index');
   RouteApi::get('/all', 'PermissionController@get_all_permission');
   RouteApi::get('/get/{id}', 'PermissionController@get_permission');
   RouteApi::post('/store', 'PermissionController@store');
   RouteApi::post('/update/{id}', 'PermissionController@update');
   RouteApi::post('/delete/{id}', 'PermissionController@delete');

   /* Role */
   RouteApi::group([
     'prefix' => 'role',]
   , function() {
     RouteApi::get('/', 'RoleController@index');
     RouteApi::get('/all', 'RoleController@get_all_role');
     RouteApi::get('/get/{id}', 'RoleController@get_role');
     RouteApi::post('/store', 'RoleController@store');
     RouteApi::post('/update/{id}', 'RoleController@update');
     RouteApi::post('/delete/{id}', 'RoleController@delete');
   });

 });
});
