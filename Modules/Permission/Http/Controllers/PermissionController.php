<?php

namespace Modules\Permission\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Entities\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return DB::table('permissions')->paginate(10);
    }

    public function get_permission(Request $request, $id) {
      $permission = DB::table('permissions')
                ->select('name')
                ->where('id', $id)
                ->first();

      if ($permission) {
        return response()->json([
          'message' => 'success',
          'status' => 'success',
          'data' => $permission,
        ]);
      }

      return response()->json([
        'message' => 'Failed get permission',
        'status' => 'error',
      ], 500);
    }

    public function get_all_permission() {
      return DB::table('permissions')->get();
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
      $name = $request->name;
      $group = '';
      $role = explode(',', $request->role);
      $user = explode(',', $request->user);
      $dependency = [];

      if ($permission = Permission::create(['name' => $name ])) {
        /* Default assign to role administrator */
        $adminRole = Role::findByName('administrator');
        $adminRole->givePermissionTo($name);

        /* Assigned Role */
        if ($role > 0) {
          foreach($role as $r) {
            if ($r == 'administrator') {
              /* Skip if admin */
              continue;
            }
            $userRole = Role::findByName($r);
            $userRole->givePermissionTo($name);
          }
        }

        /* Direct Permission to User */
        if (count($user) > 0) {
          foreach($user as $u) {
            if ($u == 1) {
              /* Skip if admin */
              continue;
            }
            $userDirect = User::find($u);
            $userDirect->givePermissionTo($name);
          }
        }

        return response()->json([
          'status' => 'success',
          'message' => 'Permission has been created successfully',
          'id' => $permission->id,
        ]);
      }

      return response()->json([
        'status' => 'error',
        'message' => 'Failed create permission',
      ], 500);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('permission::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('permission::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function delete(Request $request, $id) {
      $name = $id;
      $userHasPermission = User::permission($id)->get();
      $roleHasPermission = Role::all();

      if (count($userHasPermission) > 0) {
        // remove from user
        foreach($userHasPermission as $u) {
          $user = User::find($u->id);
          $user->revokePermissionTo($id);
        }
      }

      if (count($roleHasPermission) > 0) {
        // remove from user
        foreach($roleHasPermission as $r) {
          $role = Role::findByName($r->name);
          $role->revokePermissionTo($id);
        }
      }

      $delete = Permission::where('name', $id)
                          ->delete();

      if ($delete) {
        return response()->json([
          'message' => 'Permission has been deleted successfully',
          'status' => 'success',
        ]);
      }

      return response()->json([
        'message' => 'Failed delete permission',
        'status' => 'error',
      ], 500);
    }
}
