<?php

namespace Modules\Permission\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Modules\User\Entities\User;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return DB::table('roles')->paginate(10);
    }

    public function get_all_role() {
      return DB::table('roles')->get();
    }

    public function get_role(Request $request, $id) {
      $role = DB::table('roles')
                ->select('name')
                ->where('id', $id)
                ->first();

      if ($role) {
        return response()->json([
          'message' => 'success',
          'status' => 'success',
          'data' => $role,
        ]);
      }

      return response()->json([
        'message' => 'Failed get role',
        'status' => 'error',
      ], 500);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
      $name = $request->name;
      $permission = explode(',', $request->permission);
      $user = explode(',', $request->user);

      if ($role = Role::create(['name' => $name ])) {

        /* Assign Permission */
        $userRole = Role::findByName($name);
        if (count($permission) > 0) {
          foreach ($permission as $p) {
            $userRole->givePermissionTo($p);
          }
        }

        /* Assign to user */
        if (count($user) > 0) {
          foreach ($user as $u) {
            if ($u === 1) {
              /* Skip if admin */
              continue;
            }
            $userAssign = User::find($u);
            $userAssign->assignRole($name);
          }
        }

        return response()->json([
          'status' => 'success',
          'message' => 'Role has been created successfully',
          'id' => $role->id,
        ]);
      }

      return response()->json([
        'status' => 'error',
        'message' => 'Failed create role',
      ], 500);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('permission::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('permission::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function delete(Request $request, $name) {
      if ($name === 'administrator') {
        return response()->json([
          'message' => 'Can\'t delete role administrator',
          'status' => 'error',
        ], 500);
      }

      $role = Role::findByName($name);
      $user = User::all();
      $permission = Permission::all();
      foreach($permission as $p) {
        $role->revokePermissionTo($p->name);
      }
      foreach($user as $u) {
        $u->removeRole($name);
      }

      $delete = Role::where('name', $name)
                  ->delete();

      if ($delete) {
        return response()->json([
          'message' => 'Role has been deleted successfully',
          'status' => 'success',
        ]);
      }

      return response()->json([
        'message' => 'Failed delete role',
        'status' => 'error',
      ], 500);
    }
}
