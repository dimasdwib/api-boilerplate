<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//


RouteApi::version('v1',  function() {
  RouteApi::group(['prefix' => 'auth', 'namespace' => 'App\Http\Controllers\Auth'], function() {
   RouteApi::get('/me', 'LoginController@me');
   RouteApi::post('/login', 'LoginController@login');
   RouteApi::post('/logout', 'LoginController@logout');
 });
});
